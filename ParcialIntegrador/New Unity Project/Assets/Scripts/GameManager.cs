﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Unity;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static float vol = 1f,timer = 0f;
    public Camera playercamera;
    public GameObject player, playertp,puzzle;
    public List<string> namesounds = new List<string>();
    Sound s = new Sound();
    bool puzzletime = false, pausa = true, corutine;
    private void Awake()
    {
        

        Cursor.visible = false;
        AddSounds();
        AudioManager.instantiate.SetVolume(vol);
    }
    private void Update()
    {
        if (puzzletime)
        {
            if (!s.audioSource.isPlaying && !pausa)
            {
                if (!corutine)
                {
                    StartCoroutine(WaitTime());
                }
            }
            else
            {
                if (!s.audioSource.isPlaying)
                {
                    PauseActualSound();
                }
                else
                {
                    pausa = false;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Comma))
        {
            SceneManager.LoadScene("Puzzle 3");
        }
    }

    public void TPPlayer()
    {
        player.transform.position = playertp.transform.position;
        puzzle.SetActive(true);
        puzzletime = true;
        NewSound();
    }

    void AddSounds()
    {
        namesounds.Add("Voices");
        namesounds.Add("Creepy");
        namesounds.Add("BeepBeep");
    }

    void NewSound()
    {
        s = AudioManager.instantiate.GetSound(namesounds[Random.Range(0,3)]);
        s.audioSource.Play();
    }

    public void PauseActualSound()
    {
        AudioManager.instantiate.PauseSound(s.Name);
        pausa = true;
    }

    public void Breathing()
    {
        s = AudioManager.instantiate.GetSound("breathing");
        s.audioSource.Play();
    }

    IEnumerator WaitTime()
    {
        corutine = true;

        yield return new WaitForSeconds(5);

        NewSound();

        corutine = false;
    }

    public static void ChangeVol(float newvol)
    {
        vol = newvol;
        AudioManager.instantiate.SetVolume(vol);
    }
}
