﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TP : MonoBehaviour
{
    bool playertp = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playertp = true;
        }
    }
    
    public bool NeedTransportPlayer()
    {
        return playertp;
    }

    public void PlayerTransported()
    {
        playertp = false;
    }
}
