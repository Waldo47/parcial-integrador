﻿using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.VFX;

public class DeactivateBlock : MonoBehaviour
{
    public GameObject audioBack, audioFront;
    public GameObject block1, block2, objectpuzzle1,mannequins;
    public Light light;
    public VisualEffect effect;
    bool keydelivered=false, back=true, front=true;
    float timer;
    private void Update()
    {
        if (keydelivered)
        {
            if (timer>=1 && back)
            {
                audioBack.SetActive(true);
            }
            if (timer >= 2 && back)
            {
                Destroy(mannequins);
                audioFront.SetActive(true);
                back = false;
            }
            if (timer >= 3 && front)
            {
                
                Destroy(block1);
                Destroy(block2);
                front = false;
            }
            timer += Time.deltaTime;
        }
       
    }

    public void Activate()
    {
        Destroy(objectpuzzle1);
        light.enabled = true;
        effect.enabled = true;
        keydelivered = true;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("key"))
        {
            Activate();
        }
    }
}
