﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class DesactivateObject : MonoBehaviour
{
    public Player player;
    public Camera camera;
    public GameManager gameManager;
    AudioSource audio;
    void Update()
    {
        if (this.transform.parent.gameObject.CompareTag("InteractiveZone"))
        {
            player.movement = 2f;
            Volume volume = camera.gameObject.GetComponent<Volume>();
            Vignette vig;
            volume.profile.TryGet<Vignette>(out vig);
            vig.intensity.value = 0.2f;
            gameManager.PauseActualSound();
            gameManager.Breathing();
            Destroy(this);
        }
    }
}
