﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PortalNextLevel : MonoBehaviour
{
    public FirstPuzzleManager firstPuzzle;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            firstPuzzle.CameraColorAdjustmentPassing();
            LoadJSON.Save(GameManager.timer);
            SceneManager.LoadScene("Puzzle 2");
        }
    }
}
