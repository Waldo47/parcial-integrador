﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ChangeVolume : MonoBehaviour
{
    public Camera camera;
    public Player player;
    bool changeable = true;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (changeable == true)
            {
                player.movement -= 0.3f;
                Volume volume = camera.gameObject.GetComponent<Volume>();
                Vignette vig;
                volume.profile.TryGet<Vignette>(out vig);
                vig.intensity.value += 0.1f;
                changeable = false;
            }
            else
            {

            }
        }
    }
}
