﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.VFX;

public class FirstPuzzleManager : MonoBehaviour
{
    int lastTP;
    public Camera cameraplayer;
    TP tp1, tp2;
    StartAndFinish startAndFinish;
    public GameManager gameManager;
    Volume volumeplayer;
    public GameObject player, tpwall1, tpwall2, tppoint1, portal,blocks,lastwall;
    public VisualEffect effect;
    public Light light;
    void Awake()
    {
        tp1 = tpwall1.gameObject.GetComponent<TP>();
        tp2 = tpwall2.gameObject.GetComponent<TP>();
        startAndFinish = tppoint1.gameObject.GetComponent<StartAndFinish>();
        volumeplayer = cameraplayer.GetComponent<Volume>();
        ColorAdjustments color = new ColorAdjustments();
        volumeplayer.profile.TryGet<ColorAdjustments>(out color);
        color.active = true;
    }

    private void Start()
    {
        AudioManager.instantiate.PlaySound("backgroundfirstpuzzle");
    }

    void Update()
    {
        GameManager.timer += Time.deltaTime;
        if (lastTP == 2)
        {
            lastwall.gameObject.SetActive(true);
            lastTP++;
        }
        if (tp1.NeedTransportPlayer() || tp2.NeedTransportPlayer())
        {
            player.transform.position = startAndFinish.GivePositionToTP();
            ActivatePortal();
            tp1.PlayerTransported();
            tp2.PlayerTransported();
            gameManager.PauseActualSound();
            effect.enabled = false;
            light.enabled = false;
            blocks.SetActive(true);
            lastTP++;
        }
    }

    void ActivatePortal()
    {
        portal.SetActive(true);
    }

    public void CameraColorAdjustmentPassing()
    {
        Volume volume = new Volume();
        volume = cameraplayer.GetComponent<Volume>();
        ColorAdjustments color = new ColorAdjustments();
        volume.profile.TryGet<ColorAdjustments>(out color);
        color.active = false;
    }
}
