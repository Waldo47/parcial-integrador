﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CameraRotation : MonoBehaviour
{
    Vector2 rotateValue,suavity;

    public static float sensibility = 3.0f;

    public float softness = 2.0f;

    public bool canMove = true;
    GameObject player;

    private void Start()
    {
        player = this.transform.parent.gameObject;
    }
    void Update()
    {
        if (canMove)
        {
            var rotation = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            rotation = Vector2.Scale(rotation, new Vector2(softness * sensibility, softness * sensibility));

            suavity.x = Mathf.Lerp(suavity.x, rotation.x, 1f / softness);
            suavity.y = Mathf.Lerp(suavity.y, rotation.y, 1f / softness);

            rotateValue += suavity;
            rotateValue.y = Mathf.Clamp(rotateValue.y, -90f, 90f);

            transform.localRotation = Quaternion.AngleAxis(-rotateValue.y, Vector3.right);
            player.transform.localRotation = Quaternion.AngleAxis(rotateValue.x, player.transform.up);
        }
        else
        {
            if (!canMove)
            {

            }
        }
    }
    public static void ChangeSens(float sens) 
    { 
        sensibility = sens;
    }
}
