﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;

public class Player : MonoBehaviour
{
    Rigidbody rb;
    public float movement = 3.5f;
    public bool canMove=true;
    public bool left = false, right = false, up = false, down = true;
    public Camera camerafront;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Movement();
    }

    void Movement()
    {
        if (canMove)
        {
            if (down)
            {
                float xmovement = Input.GetAxis("Horizontal") * movement;
                float zmovement = Input.GetAxis("Vertical") * movement;

                xmovement *= Time.deltaTime;
                zmovement *= Time.deltaTime;

                transform.Translate(xmovement, 0, zmovement);
            }
            if (left)
            {
                float xmovement = Input.GetAxis("Horizontal") * movement;
                float zmovement = Input.GetAxis("Vertical") * movement;

                xmovement *= Time.deltaTime;
                zmovement *= Time.deltaTime;

                transform.Translate(0, -xmovement, zmovement);
            }

            if (right)
            {
                float xmovement = Input.GetAxis("Horizontal") * movement;
                float zmovement = Input.GetAxis("Vertical") * movement;

                xmovement *= Time.deltaTime;
                zmovement *= Time.deltaTime;

                transform.Translate(0, xmovement, zmovement);
            }
            if (up)
            {
                float xmovement = Input.GetAxis("Horizontal") * movement;
                float zmovement = Input.GetAxis("Vertical") * movement;

                xmovement *= Time.deltaTime;
                zmovement *= Time.deltaTime;

                transform.Translate(xmovement,0 , zmovement);
            }
        }
    }


    public void ChangeGravity(string wichway)
    {
        switch (wichway)
        {
            case "left":
                left = true;
                right = false;
                down = false;
                up = false;
                Debug.Log("Left");
                
                break;
            case "right":
                left = false;
                right = true;
                down = false;
                up = false;
                
                break;
            case "up":
                left = false;
                right = false;
                down = false;
                up = true;
                
                break;
            case "down":
                left = false;
                right = false;
                down = true;
                up = false;
                
                break;
        }
    }
}
