﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObject : MonoBehaviour
{
    public GameObject ObjectToPickUp;
    public GameObject PickedObject;
    public Transform interactionZone;
    private void Start()
    {
        
    }

    private void Update()
    {
        if (ObjectToPickUp != null && ObjectToPickUp.GetComponent<Pickeable>().isPickable == true && PickedObject ==null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                PickObject();
            }
        }
        else if (PickedObject != null)
        {
            if (Input.GetMouseButtonDown(1))
            {
                DropObject();
            }
        }
    }


    void PickObject()
    {
        PickedObject = ObjectToPickUp;
        PickedObject.GetComponent<Pickeable>().isPickable = false;
        PickedObject.transform.SetParent(interactionZone);
        PickedObject.transform.position = interactionZone.position;
        PickedObject.GetComponent<Rigidbody>().useGravity = false;
        PickedObject.GetComponent<Rigidbody>().isKinematic = true;
    }

    void DropObject()
    {
        if (PickedObject.gameObject.CompareTag("spherekey")|| PickedObject.gameObject.CompareTag("key"))
        {
            PickedObject.GetComponent<Pickeable>().isPickable = true;
            PickedObject.transform.SetParent(null);
            PickedObject.GetComponent<Rigidbody>().useGravity = false;
            PickedObject.GetComponent<Rigidbody>().isKinematic = true;
            PickedObject = null;
        }
        else
        {
            PickedObject.GetComponent<Pickeable>().isPickable = true;
            PickedObject.transform.SetParent(null);
            PickedObject.GetComponent<Rigidbody>().useGravity = true;
            PickedObject.GetComponent<Rigidbody>().isKinematic = false;
            PickedObject = null;
        }
    }
}
