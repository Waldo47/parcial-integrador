﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeNumberTutorial : MonoBehaviour
{
    public CubeNumbers cubeNumbers;
    public GameObject dissapearwall;

    private void Update()
    {
        if (cubeNumbers.ReturnOkay())
        {
            Destroy(dissapearwall.gameObject);
        }
    }

}
