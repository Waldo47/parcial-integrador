﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSpikes : MonoBehaviour
{
    public GameObject walldissapear;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("spherekey"))
        {
            Destroy(other.gameObject);
            Destroy(walldissapear.gameObject);
            Destroy(this.gameObject);
        }
    }
}
