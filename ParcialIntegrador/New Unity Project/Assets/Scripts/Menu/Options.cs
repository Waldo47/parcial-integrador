﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public Slider volume, sensitivity;

    private void Awake()
    {
        volume.value = GameManager.vol;
        sensitivity.value = CameraRotation.sensibility;
    }

    public void ChangeVolume()
    {
        GameManager.ChangeVol(volume.value);
        AudioManager.instantiate.SetVolume(volume.value);
    }
    
    public void ChangeSensitivity()
    {
        CameraRotation.sensibility = sensitivity.value;

    }
}
