﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour
{
    public Animator animator;

    public void Dance()
    {
        animator.SetBool("Dance", true);
        animator.SetBool("KeepDancing", true);
    }

    public void ChangeAnimation(int number)
    {
        Debug.Log(number);
        switch (number)
        {
            case 1:
                animator.SetBool("TPoseIdle", false);
                animator.SetBool("IdleTpose", true);
                break;
            case 2:
                animator.SetBool("TPoseIdle", true);
                animator.SetBool("IdleTpose", false);
                break;
        }
    }

}
