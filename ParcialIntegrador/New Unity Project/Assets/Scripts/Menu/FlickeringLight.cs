﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class FlickeringLight : MonoBehaviour
{
    public Light light;
    public float count = 0;
    Color normal;
    public Animation animation;

    private void Awake()
    {
        normal = light.color;
    }
    private void Start()
    {
        StartCoroutine(Flickering());
    }

    private void Update()
    {

        if (count >= 12)
        {
            StartCoroutine(Flickering());
            count = 0;
        }

        count += Time.deltaTime;
    }

    IEnumerator Flickering()
    {
        light.intensity = 0;

        animation.ChangeAnimation(Random.Range(1, 3));

        yield return new WaitForSeconds(0.5f);

        light.intensity = 1000;

        yield return new WaitForSeconds(0.1f);

        light.intensity = 0;

        yield return new WaitForSeconds(0.1f);

        light.intensity = 1000;

        yield return new WaitForSeconds(0.2f);

        light.intensity = 0;

        yield return new WaitForSeconds(0.7f);

        light.intensity = 1000;

        ChangeColour(Random.Range(0,7));

        yield return null;
    }

    void ChangeColour(int random)
    {
        switch (random)
        {
            case 1:
                light.color = Color.red;
                break;
            case 2:
                light.color = normal;
                break;
            case 3:
                light.color = Color.magenta;
                break;
            default:
                light.color = normal;
                break;
        }
    }
}
