﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Unity;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour
{
    public GameObject menu,options;
    public Player player;
    public CameraRotation camera;
    bool inPause=false;
    private void Awake()
    {
    }

    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!inPause)
            {
                Cursor.visible = true;     
                player.canMove = false;
                camera.canMove = false;
                menu.SetActive(true);
                inPause = true;
            }
            else
            {
                if (inPause)
                {
                    Cursor.visible = false;
                    player.canMove = true;
                    camera.canMove = true;
                    menu.SetActive(false);
                    options.SetActive(false);
                    inPause = false;
                }
            }
        }
    }

    public void Options()
    {
        menu.SetActive(false);
        options.SetActive(true);
    }

    public void Continue()
    {
        Cursor.visible = false;
        inPause = false;
        player.canMove = true;
        camera.canMove = true;
        menu.SetActive(false);
    }

    public void MainMenu()
    {
        AudioManager.instantiate.StopAllSound();
        SceneManager.LoadScene("Menu");
    }

    public void Back()
    {
        options.SetActive(false);
        menu.SetActive(true);
    }
}
