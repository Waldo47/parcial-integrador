﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class MenuOptions : MonoBehaviour
{
    public GameObject menu,options;
    public Text timer;
    public static bool gameinitiated = false;
    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (!gameinitiated)
        {
            LoadJSON.Save(0);
            gameinitiated = true;
        }
    }
    private void Update()
    {
        timer.text = LoadJSON.LoadTimer().ToString();  
    }
    public void StartGame()
    {
        SceneManager.LoadScene("Puzzle 1");
    }

    public void Options()
    {
        menu.SetActive(false);
        options.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void Return()
    {
        menu.SetActive(true);
        options.SetActive(false);
    }

    public void Tutorial()
    {
        Cursor.visible = false;
        SceneManager.LoadScene("Tutorial");
    }
}
