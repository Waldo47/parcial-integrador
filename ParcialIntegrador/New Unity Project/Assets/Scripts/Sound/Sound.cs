﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[System.Serializable]
public class Sound
{
    public AudioClip ClipSound;
    public string Name;

    [Range(0f, 1f)]
    public float Volume;

    [Range(0f, 1f)]
    public float pitch;

    public bool repeat;

    [HideInInspector]
    public AudioSource audioSource;

    
}
