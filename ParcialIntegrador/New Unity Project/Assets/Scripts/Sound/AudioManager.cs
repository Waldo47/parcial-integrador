﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instantiate;

    private void Awake()
    {
        if (instantiate == null)
        {
            instantiate = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.audioSource = gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.ClipSound;
            s.audioSource.volume = GameManager.vol;
            s.audioSource.pitch = s.pitch;
            s.audioSource.loop = s.repeat;
        }
    }

    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }
        else
        {
            s.audioSource.Play();
        }
    }

    public void PauseSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }
        else
        {
            s.audioSource.Pause();
        }
    }

    public Sound GetSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return null;
        }
        else
        {
            s.audioSource.Pause();
            return s;
        }
    }

    public void SetVolume(float newvolume)
    {
        foreach (Sound s in sounds)
        {
            s.audioSource.volume = newvolume;
        }
    }

    public void StopAllSound()
    {
        foreach (Sound s in sounds)
        {
            s.audioSource.Stop();
        }
    }
}
