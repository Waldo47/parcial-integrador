﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portals : MonoBehaviour
{
    public GameObject reference,portalroom,player1,player2,player2position;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            portalroom.SetActive(true);
            other.transform.position = reference.transform.position;
        }
    }
}
