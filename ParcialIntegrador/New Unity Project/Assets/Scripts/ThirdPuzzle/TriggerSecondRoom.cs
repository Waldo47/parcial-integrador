﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSecondRoom : MonoBehaviour
{
    public Camera cameraplayer;
    public GameObject audiosource1, wall;

    private void OnTriggerEnter(Collider other)
    {
        AudioManager.instantiate.StopAllSound();
        audiosource1.SetActive(true);
        wall.SetActive(true);
    }
}
