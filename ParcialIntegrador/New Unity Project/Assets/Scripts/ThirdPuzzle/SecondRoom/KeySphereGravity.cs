﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class KeySphereGravity : MonoBehaviour
{
    public GameObject gravity;
    public VisualEffect vfx;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("spherekey"))
        {
            gravity.SetActive(true);
            vfx.gameObject.SetActive(true);
            Destroy(other.gameObject);
            Destroy(this);
            
        }
    }
}
