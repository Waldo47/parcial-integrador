﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.VFX;
using UnityEngine;

public class KeySphereRoom2 : MonoBehaviour
{
    public Light light;
    public VisualEffect vFXParticle;

    public GameObject wall1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("spherekey"))
        {
            light.gameObject.SetActive(true);
            vFXParticle.gameObject.SetActive(true);
            Destroy(other.gameObject);
            Destroy(wall1.gameObject);
            Destroy(this);
        }
    }
}
