﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class KeySphereWallGravity : MonoBehaviour
{
    public GameObject wall;
    public VisualEffect vfx;
    public List<GameObject> gravity = new List<GameObject>();
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("spherekey"))
        {
            wall.SetActive(false);
            vfx.gameObject.SetActive(true);
            Destroy(other.gameObject);
            Physics.gravity = new Vector3(0,-9.8f,0);
            foreach (GameObject gb in gravity)
            {
                gb.SetActive(false);
            }
            Destroy(this);
        }
    }
}
