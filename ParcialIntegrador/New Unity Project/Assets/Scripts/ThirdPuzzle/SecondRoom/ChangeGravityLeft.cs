﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGravityLeft : MonoBehaviour
{
    public Player player;
    public Camera reference;
    public Vector3 newgravity;
    public string gravity;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Physics.gravity = newgravity;
            player.ChangeGravity(gravity);
            other.transform.position = reference.transform.position;
;       }
    }
}
