﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalLastRoom : MonoBehaviour
{
    public GameObject player1,room;
    public ThirdPuzzleManager thirdPuzzle;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player1.SetActive(true);
            thirdPuzzle.doorcount++;
            Destroy(room);
        }
    }
}
