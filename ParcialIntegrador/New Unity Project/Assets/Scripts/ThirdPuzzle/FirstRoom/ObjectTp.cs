﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTp : MonoBehaviour
{
    public GameObject tpplace,playerhands;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("spherekey"))
        {
            Pickeable pickeable;
            pickeable = other.gameObject.GetComponent<Pickeable>();
            pickeable.isPickable = false;
            other.gameObject.transform.position = playerhands.transform.position;
            other.gameObject.transform.SetParent(playerhands.transform);
            pickeable.isPickable = true;
        }
    }
}
