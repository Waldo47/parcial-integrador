﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;

public class TriggerGuys : MonoBehaviour
{
    Player playerscript;
    public ThirdPuzzleManager puzzleManager;
    public GameObject guys, portal1place,playerref,player,portal;
    float timer;
    bool timeractive = false;


    private void Update()
    {
        if (timeractive)
        {
            if (timer >= 15)
            {
                puzzleManager.doorcount++;
                player.transform.position = playerref.transform.position;
                playerscript.canMove = true;
                portal.SetActive(false);
                Destroy(portal1place.gameObject);
            }

            timer += Time.deltaTime;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            guys.SetActive(true);
            timeractive = true;
            player = other.gameObject;
            playerscript = player.gameObject.GetComponent<Player>();
            playerscript.canMove = false;
        }
    }
}
