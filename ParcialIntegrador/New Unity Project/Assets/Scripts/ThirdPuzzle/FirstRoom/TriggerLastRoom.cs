﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerLastRoom : MonoBehaviour
{
    Player player;
    public GameObject secondplayer,reference;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(secondplayer.gameObject);
            player = other.gameObject.GetComponent<Player>();
            player.canMove = true;
        }
    }
}
