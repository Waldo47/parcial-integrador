﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class KeySphere : MonoBehaviour
{
    public Light light;
    public VisualEffect vFXParticle;

    public GameObject wall1, wall2;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("spherekey"))
        {
            light.gameObject.SetActive(true);
            vFXParticle.gameObject.SetActive(true);
            Destroy(other.gameObject);
            Destroy(wall1.gameObject);
            Destroy(wall2.gameObject);
            Destroy(this);
        }
    }
}
