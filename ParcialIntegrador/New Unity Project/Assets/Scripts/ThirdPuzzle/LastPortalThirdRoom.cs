﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LastPortalThirdRoom : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Cursor.visible = true;
            LoadJSON.Save(GameManager.timer);
            Physics.gravity = new Vector3(0,-9.8f,0);
            SceneManager.LoadScene("Menu");
        }
    }
}
