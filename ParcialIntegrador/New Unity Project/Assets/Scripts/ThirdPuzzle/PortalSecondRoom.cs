﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalSecondRoom : MonoBehaviour
{
    public GameObject portalroom, playerref;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            portalroom.SetActive(true);
            other.transform.position = playerref.transform.position;
            other.gameObject.SetActive(false);
        }
    }
}
