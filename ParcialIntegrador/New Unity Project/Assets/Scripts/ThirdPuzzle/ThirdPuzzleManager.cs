﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPuzzleManager : MonoBehaviour
{
    public GameObject door,portal1,portal2;
    public int doorcount = 0;


    private void Awake()
    {
        AudioManager.instantiate.StopAllSound();
        AudioManager.instantiate.PlaySound("backgroundthirdpuzzle");
        Cursor.visible = false;
    }


    private void Update()
    {
        if (doorcount == 1)
        {
            portal1.SetActive(false);
            portal2.SetActive(true);
        }
        else
        {
            if (doorcount == 2)
            {
                portal1.SetActive(false);
                portal2.SetActive(false);
                door.SetActive(false);
            }
        }
    }
}
