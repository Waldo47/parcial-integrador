﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SecondPuzzleManager : MonoBehaviour
{
    public GameObject lastPortal;
    public List<GameObject> cubesWithNumbers = new List<GameObject>();
    public List<AudioClip> sounds = new List<AudioClip>();
    bool passokay;
    int countpass;

    private void Awake()
    {
        AudioManager.instantiate.StopAllSound();
        AudioManager.instantiate.PlaySound("backgroundsecondpuzzle");
    }
    private void Update()
    {
        GameManager.timer += Time.deltaTime;
        if (CheckPass())
        {
            AudioManager.instantiate.StopAllSound();
            lastPortal.SetActive(true);
        }
    }

    bool CheckPass()
    {
        countpass = 0;
        foreach (GameObject c in cubesWithNumbers)
        {
            CubeNumbers cubeNumbers = c.gameObject.GetComponent<CubeNumbers>();
            if (cubeNumbers.ReturnOkay())
            {
                countpass++;
            }
        }
        if (countpass == 4)
        {
            passokay = true;
        }
        return passokay;
    }

    public AudioClip ReturnNumberSound(int pos)
    {
        AudioClip s;
        s = sounds[pos];
        return s;
    }
}
