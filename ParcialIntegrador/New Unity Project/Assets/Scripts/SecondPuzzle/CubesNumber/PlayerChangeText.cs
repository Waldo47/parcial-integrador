﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChangeText : MonoBehaviour
{
    public Camera camera;
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            var ray = camera.ViewportPointToRay(Vector3.one * 0.5f);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 2f))
            {
                if (hit.transform.gameObject.tag == "TextChangeable")
                {
                    CubeNumbers text = hit.transform.gameObject.GetComponent<CubeNumbers>();
                    text.ChangeText();
                }
            }

        }
    }
}
