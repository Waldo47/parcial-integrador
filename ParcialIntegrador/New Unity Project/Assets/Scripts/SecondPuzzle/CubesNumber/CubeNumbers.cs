﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CubeNumbers : MonoBehaviour
{
    public GameObject changeabletext;
    public int passwordnumber;
    TextMeshPro text;
    RawImage image;
    private void Awake()
    {
        passwordnumber = UnityEngine.Random.Range(0,9);
        text = changeabletext.gameObject.GetComponent<TextMeshPro>();
    }

    public void ChangeText()
    {
        int numero = Int32.Parse(text.text);
        if (numero >= 9)
        {
            text.text = "0";
        }
        else
        {
            
            numero++;
            text.text = numero.ToString();
        }
    }

    public bool ReturnOkay()
    {
        bool passwordokay;
        if (Int32.Parse(text.text) == passwordnumber)
        {
            passwordokay = true;
        }
        else
        {
            passwordokay = false;
        }
        return passwordokay;
    }

    public int ReturnPassword()
    {
        return passwordnumber;
    }
}
