﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LastPortal : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag("Player"))
        {
            AudioManager.instantiate.StopAllSound();
            LoadJSON.Save(GameManager.timer);
            Cursor.visible = true;
            SceneManager.LoadScene("Puzzle 3");
        }
    }
    
}
