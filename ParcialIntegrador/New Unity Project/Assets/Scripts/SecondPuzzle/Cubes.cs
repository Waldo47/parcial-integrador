﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Cubes : MonoBehaviour
{
    public GameObject forcefield;
    Pickeable pickeable;
    public SecondPuzzleManager puzzleManager;
    public CubeNumbers cubeNumbers;
    public VisualEffect particle;
    AudioSource audioSource;
    AudioClip password;
    bool corutine,inplace = false;
    float timer;
    private void Awake()
    {
        pickeable = this.gameObject.GetComponent<Pickeable>();
        audioSource = this.gameObject.GetComponent<AudioSource>();
        particle.Stop();
    }

    private void Start()
    {
        password = puzzleManager.ReturnNumberSound(cubeNumbers.ReturnPassword());
        audioSource.clip = password;
    }

    private void Update()
    {
        if (timer > 5)
        {
            if (inplace)
            {
                if (!corutine)
                {
                    StartCoroutine(WaitTime());
                }
            }
            timer = 0;
        }
        else
        {

        }
        timer += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == this.gameObject.name)
        {
            inplace = true;
            pickeable.isPickable = false;
            forcefield.SetActive(true);
            particle.Play();
            audioSource.Play();
        }
    }

    void PlaySound()
    {
        audioSource.Play();
    }

    IEnumerator WaitTime()
    {
        corutine = true;

        yield return new WaitForSeconds(5);

        PlaySound();

        corutine = false;
    }
}

