﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using UnityEngine;

public class LoadJSON : MonoBehaviour
{
    
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    [System.Serializable]
    public class Parameters
    {
        public float timer;
    }

    [System.Serializable]
    public class ListParameters
    {
        public List<Parameters> LstParameters = new List<Parameters>();
    }

    private ListParameters obj = new ListParameters();

    public static ListParameters returnlistParameters()
    {
        string route = Application.dataPath + "\\config";
        if (File.Exists(route))
        {
            WWW www = new WWW("file:///" + route);
            if (www != null)
            {
                return JsonUtility.FromJson<ListParameters>(www.text);
            }
        }
        return new ListParameters();
    }

    public static void Save(float time)
    {
        ListParameters obj = new ListParameters();
        Parameters par = new Parameters();
        par.timer = time;
        obj.LstParameters.Clear();
        obj.LstParameters.Add(par);
        string json = JsonUtility.ToJson(obj);
        var routefile = Application.dataPath + "\\config";
        if (!File.Exists(routefile))
        {
            File.Create(routefile).Dispose();
        }
        File.WriteAllText(routefile, json);
    }

    public static float LoadTimer()
    {
        ListParameters obj = returnlistParameters();
        
        return obj.LstParameters[0].timer;
    }
}
