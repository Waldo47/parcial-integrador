﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Troll : MonoBehaviour
{
    public List<GameObject> avatars = new List<GameObject>();

    private void Awake()
    {
        foreach (GameObject item in avatars)
        {
            Animation animation = item.gameObject.GetComponent<Animation>();
            animation.Dance();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
